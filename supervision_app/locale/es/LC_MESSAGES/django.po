# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-07-21 15:07-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: supervision_app/apps.py:33
#: supervision_app/templates/supervision_agentes.html:48
msgid "Agentes"
msgstr ""

#: supervision_app/apps.py:38
#: supervision_app/templates/supervision_campanas_entrantes.html:32
msgid "Campañas Entrantes"
msgstr ""

#: supervision_app/apps.py:43
#: supervision_app/templates/supervision_campanas_salientes.html:31
msgid "Campañas Salientes"
msgstr ""

#: supervision_app/apps.py:49
msgid "Supervisión"
msgstr ""

#: supervision_app/apps.py:68
msgid "Estado de agentes en supervisión"
msgstr ""

#: supervision_app/apps.py:70
msgid "Estado de campañas entrantes en supervisión"
msgstr ""

#: supervision_app/apps.py:72
msgid "Estado de campañas salientes en supervision"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:75
msgid "Filtrar grupo"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:85
msgid "Filtrar Campaña"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:95
msgid "Finalizar"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:104
#: supervision_app/templates/supervision_campanas_entrantes.html:37
#: supervision_app/templates/supervision_campanas_salientes.html:36
msgid "Nombre"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:107
msgid "Status"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:108
msgid "Tiempo"
msgstr ""

#: supervision_app/templates/supervision_agentes.html:109
msgid "Acciones"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:38
msgid "Llamadas"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:39
#: supervision_app/templates/supervision_campanas_salientes.html:38
msgid "Atendidas"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:40
msgid "Abandonadas"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:41
msgid "Abandonadas en anuncio"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:42
msgid "T. promedio espera"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:43
msgid "Expiradas"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:44
msgid "En espera"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:45
msgid "T. promedio abandono"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:46
#: supervision_app/templates/supervision_campanas_salientes.html:40
msgid "Gestiones"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:57
msgid "segundos"
msgstr ""

#: supervision_app/templates/supervision_campanas_entrantes.html:60
msgid " segundos"
msgstr ""

#: supervision_app/templates/supervision_campanas_salientes.html:37
msgid "Llamadas discadas"
msgstr ""

#: supervision_app/templates/supervision_campanas_salientes.html:39
msgid "No atendidas"
msgstr ""
